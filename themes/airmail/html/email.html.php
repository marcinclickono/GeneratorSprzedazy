<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/airmail/css/airmail.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body>
		<table cellspacing="0" border="0" cellpadding="0" width="100%" align="center" style="margin: 0px;" >
			<tbody>
				<tr valign="top">
					<td><!--container-->
						<table cellspacing="0" cellpadding="0" border="0" align="center" width="621" height="77">
							<tbody>
								<tr>
									<td valign="top" height="19">&nbsp;</td>
								</tr>
								<tr>
									<td class="pgora" valign="middle" height="23" style="vertical-align:middle !important;">
										<p style="margin:0; padding:0; color:#766d59; font-size: 12px;">
											
										</p>
									</td>
								</tr>
								<tr>
									<td valign="top" height="1" style="margin: 0; padding: 0;"><img width="621" height="1" src="images/spacer-2.jpg" alt="" style="display:block;"/></td>
								</tr>
								<tr>
									<td class="pgora" valign="middle" height="23" style="vertical-align:middle !important;">
										<p style="margin:0; padding:0; color:#766d59; font-size: 12px;">
										<?php $view['slots']->output('top1'); ?>
										</p>
									</td>
								</tr>
								<tr>
									<td valign="top" height="1" style="margin: 0; padding: 0;"><img width="621" height="1" src="images/spacer-2.jpg" alt="" style="display:block;"/></td>
								</tr>
								<tr>
									<td valign="top" height="10">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table class="bgstampp" cellspacing="0" cellpadding="0" border="11" align="center" width="621" bgcolor="#f7f3e6" style="border-width:11px; border-color:#ffffff; border-style:solid; background-color:#f7f3e6; background-position: center top; background-repeat: repeat-x no-repeat;">
							<tbody>
								<tr>
									<td valign="top" border="0" style="border: none; ">
										<table cellspacing="0" cellpadding="0" border="0" align="center" style="padding-bottom: 13px;">
											<tbody>
												<tr>
													<td class="headertop" valign="top" colspan="2">
													
													
													
													</td>
												</tr>
												<tr>
													<td class="stamp" valign="top" width="511" style="padding-top: 19px; padding-left: 21px;">
													<h1 style="margin: 0; font-size: 42px; color:#33384f;">
													<?php $view['slots']->output('top2'); ?>
													</h1>
													</td>
													<td valign="top" width="88">
													
													<div class="stampbell"></div>
													
													
													</td>
												</tr>
												<tr>
													<td valign="top" colspan="2">
													
													<div class="doublespacer"></div>
													
													</td>
												</tr>
											</tbody>
										</table>
							<!--content--><table cellspacing="0" cellpadding="0" border="0" align="center" width="597" style="padding-bottom: 20px;">
											<tbody>
												<tr>
													<td valign="top">
														<table cellspacing="0" cellpadding="0" border="0" align="center" width="597">
															<tbody>
																<tr>
																	<td valign="top" style="padding-top: 6px; padding-bottom: 5px; padding-left: 23px; background-image: url('images/date-bg-3.jpg'); background-repeat: repeat-x; background-position: center;" bgcolor="#545D6C" background="images/date-bg-3.jpg">
																		<h3 class="date" style="margin:0; color:#ffffff; font-size: 10px; font-weight: bold; text-transform: uppercase; background-color: #545D6C; "><currentmonthname> <currentday>, <currentyear></h3>
																	</td>
																</tr>
															</tbody>
														</table>
														<table cellspacing="0" cellpadding="0" border="0" align="center" width="551" style="">
															<tbody>
																<tr>
																	<td valign="top" style="padding-top: 10px; padding-bottom: 19px;">
																		<table cellspacing="0" cellpadding="0" border="0" align="center">
																			<tbody>
																				<tr>
																					<td valign="top">
																					
																						<h2 style="margin: 0; padding-bottom: 5px; font-size: 22px; font-family: Georgia; font-style: italic; font-weight: lighter; color: #853938;">
																						
																						<?php $view['slots']->output('top3'); ?>
																						
																						<div class="tpspacer3"></div>
																						
																						
																						</h2>
																						
																					</td>
																				</tr>
																				<tr>
																					<td valign="top" height="5">
																					
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						<p style="text-align:justify; font-size: 12px; line-height: 25px; color: #524e47;">
																							<?php $view['slots']->output('top4'); ?>
																						</p>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																
																	
																	<td>
																	<div class="topspacer3"></div>
																
																	
																	</td>
																</tr>
																<tr>
																	<td valign="top" style="padding-top: 10px; padding-bottom: 19px;">
																		<table cellspacing="0" cellpadding="0" border="0" align="center">
																			<tbody>
																				<tr>
																					<td valign="top">
																					<h2 style="margin: 0; padding-bottom: 5px; font-size: 22px; font-family: Georgia; font-style: italic; font-weight: lighter; color: #853938;">
																					
																						<?php $view['slots']->output('top5'); ?>
																					
																					</h2>
																				
																					
																					</td>
																				</tr>
																				<tr>
																			<td valign="top" height="5">
																				
																				
																				
																				<div class="tpspacer3"></div>
																			</td>
																				
																				</tr>
																				<tr>
																					<td valign="top">
																					
																						<p style="text-align:justify; font-size: 12px; line-height: 25px; color: #524e47;">
																						
																						</p>
																						
																						<?php $view['slots']->output('top6'); ?>
																													
																					</td>
																				
																					
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td valign="top">
																	
																	<div class="topspacer3"></div>
														
																	
																	</td>
																</tr>
																<tr>
																	<td valign="top" style="padding-top: 10px; padding-bottom: 19px;">
																		<table cellspacing="0" cellpadding="0" border="0" align="center">
																			<tbody>
																				<tr>
																					<td valign="top">
																						<h2 style="margin: 0; padding-bottom: 5px; font-size: 22px; font-family: Georgia; font-style: italic; font-weight: lighter; color: #853938;">
																						
																						<?php $view['slots']->output('bottom1'); ?>
																						
																						</h2>
																						
																						
																					</td>
																				</tr>
																				<tr><td valign="top" height="5">
																				
																				
																				<div class="tpspacer3"></div>
																				</td></tr>
																				<tr>
																					<td valign="top">
																						<table cellspacing="0" cellpadding="0" border="0" align="center">
																							<tbody>
																								<tr>
																									<td valign="top" colspan="2">
																										<p style="text-align:justify; font-size: 12px; line-height: 25px; color: #524e47;"><?php $view['slots']->output('bottom2'); ?></p>
																									</td>
																								</tr>
																								<tr>
																									<td valign="top" width="345">
																									
																									
																									<?php $view['slots']->output('bottom5'); ?>
																									
																									</td>
																									<td>
																									
																										<p style="text-align:justify; font-size: 12px; line-height: 25px; color: #524e47;">
																										<?php $view['slots']->output('bottom3'); ?>
																										</p>
																									
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td valign="top">
																		<div class="topspacer3"></div>
																
																	
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table><!--/content-->
									</td>
								</tr>
								<tr>
									<td valign="top" border="0" bgcolor="#f7f3e6" height="124" style="height: 124px; border: none; margin: 0px; background-color: #f7f3e6;">
										<table cellspacing="0" cellpadding="0" border="0" align="center" width="597">
											<tbody>
												<tr>
													<td valign="top" colspan="2">
													
													<div class="bottomdbspacer"></div>
													
													</td>
												</tr>
												<tr>
													<td valign="top" background="http://newsletters.urldock.com/stan-cm3/images/footer-bg.jpg" style="padding-top: 14px; padding-left: 24px;">
														<p style="font-size: 11px; line-height:16px; color:#766d59;">
															<?php $view['slots']->output('bottom4'); ?>
														</p>
													</td>
													<td valign="top" width="189" style="padding:0; margin:0;">
													
													<div class="returned"></div>
													
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<!--/container-->
					</td>
				</tr>
			</tbody>
		</table>

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>