<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend(":$template:base.html.php");
$parentVariant = $page->getVariantParent();
$title         = (!empty($parentVariant)) ? $parentVariant->getTitle() : $page->getTitle();
$view['slots']->set('public', (isset($public) && $public === true) ? true : false);
$view['slots']->set('pageTitle', $title);
?>

	
<!-- Header -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <h1><?php $view['slots']->output('top1'); ?></h1>
            <h3><?php $view['slots']->output('top2'); ?></h3>
            <br>
        </div>
    </header>	
	
<!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php $view['slots']->output('top3'); ?></h2>
                    <p class="lead"><?php $view['slots']->output('top4'); ?></p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
	
	<!-- Services -->
    <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
    <section id="services" class="services bg-primary">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2><?php $view['slots']->output('top5'); ?></h2>
                  
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">	
                                <h4>
                                    <strong><?php $view['slots']->output('top6'); ?></strong>
                                </h4>
                                <p><?php $view['slots']->output('top7'); ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                
                                <h4>
                                    <strong><?php $view['slots']->output('top8'); ?></strong>
                                </h4>
                                <p><?php $view['slots']->output('top9'); ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                               
                                <h4>
                                    <strong><?php $view['slots']->output('top10'); ?></strong>
                                </h4>
                                <p><?php $view['slots']->output('top11'); ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                               
                                <h4>
                                    <strong><?php $view['slots']->output('top12'); ?></strong>
                                </h4>
                                <p><?php $view['slots']->output('top13'); ?></p>
                               
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
	
	<!-- Callout -->
    <aside class="callout">
        <div class="text-vertical-center">
            <h1><?php $view['slots']->output('bottom1'); ?></h1>
        </div>
    </aside>
	
	 <!-- Portfolio -->
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2><?php $view['slots']->output('bottom2'); ?></h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <?php $view['slots']->output('bottom3'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
								<?php $view['slots']->output('bottom4'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
								<?php $view['slots']->output('bottom5'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <?php $view['slots']->output('bottom6'); ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                  
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
	
	    <!-- Call to Action -->
    <aside class="call-to-action bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3><?php $view['slots']->output('bottom7'); ?></h3>
                </div>
            </div>
        </div>
    </aside>


	
	<!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h4><strong><?php $view['slots']->output('bottom8'); ?></strong>
                    </h4>
                    <p><?php $view['slots']->output('bottom9'); ?></p>
                    <br>
                </div>
            </div>
        </div>
    </footer>


<?php $view['slots']->output('builder'); ?>	
	
	
	
	