<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Campaign Monitor Newsletter</title>
	<style>
	a:hover { text-decoration: none !important; }
	.header h1 {color: #055d90; font: normal 39px Georgia, Times, serif; margin: 0; padding: 0; line-height: 39px;}
	.header p {color: #cf373e; font: normal 17px Georgia, Times, serif; margin: 0; padding: 0; line-height: 12px; font-style: italic;}
	.header .subscribe p {font-size: 12px; line-height: 18px; color:#585858; margin: 0; padding: 0;font-family: Georgia, Times, serif;}
	.header .subscribe p a{ color: #c94545;}
	
	.sidebar p {color:#585858; font-weight: normal; margin: 0; padding: 0; line-height: 15px; font-size: 13px;font-family: Georgia, Times, serif; }
	.sidebar p a{color: #cf373e; text-decoration: underline;}
	.content h2 {color:#055d90; font-weight: normal; margin: 0; padding: 0; line-height: 26px; font-size: 25px;font-family: Georgia, Times, serif;  }
	.content p {color:#585858; font-weight: normal; margin: 0; padding: 0; line-height: 20px; font-size: 14px;font-family: Georgia, Times, serif; }
	.content a {color: #cf373e;}
	.footer p {font-size: 11px;  line-height: 18px; color:#616161; margin: 0; padding: 0;font-family: Georgia, Times, serif;}
	.footer a {color: #c94545; }
	.case {background: #e8e6e0 url('images/bg_email.png'); } 
	</style>
</head>
<body bgcolor="#e4e4e4" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased;width:100% !important;background:#e4e4e4;-webkit-text-size-adjust:none;">
        <?php $view['assets']->outputScripts("bodyOpen"); ?>
        <?php $view['slots']->output('_content'); ?>
        <?php $view['assets']->outputScripts("bodyClose"); ?>
    </body>
</html>