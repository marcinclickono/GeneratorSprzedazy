<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/worn/css/worn.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body style="margin: 0; padding: 0;" class="body">

<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
          <tr>
               <td align="center" style="padding: 15px 0;" class="case">
                <!--[if gte mso 9]>
            <td align="center" style="padding: 15px 0; background: none;">
         <![endif]-->
			    <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: Georgia, Times, serif;" class="header">
			      	<tr>
				        <td align="center" style="padding: 5px 0 10px; font-size: 11px; color:#585858; margin: 0; line-height: 1.4;font-family: Georgia, Times, serif;" valign="top" colspan="2" class="subscribe">
							<p style="font-size: 12px; line-height: 18px; color:#585858; margin: 0; padding: 0;font-family: Georgia, Times, serif;">
								<?php $view['slots']->output('top1'); ?>
							</p>	
						</td>
				   </tr>
				  	<tr>
				        <td align="left" colspan="2" style="height:16px; font-size: 1px; line-height: 1px" height="16" valign="top">
						
							<div class="divider_wide"></div>
							
						</td>
						
				     </tr>
				  <tr>
				    <td align="left" style="padding: 25px 0 15px;" width="385">
						<h1 style="color: #055d90; font: normal 39px Georgia, Times, serif; margin: 0; padding: 0; line-height: 39px;">
							<?php $view['slots']->output('top2'); ?>
						</h1>
						<p style="color: #cf373e; font: normal 17px Georgia, Times, serif; margin: 0; padding: 0; line-height: 12px; font-style: italic;">
							<?php $view['slots']->output('top3'); ?>
						</p>
			        </td>
					<td align="right" style="padding: 15px 0 0;" width="215">
					
						<div class="imgclass"><?php $view['slots']->output('img0'); ?></div>
						
			        </td>
			      </tr>
				</table><!-- header-->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: Georgia, Times, serif; ">
			      	
					<tr>
					<td width="70" style="font-size: 1px;" style="font-family: Georgia, Times, serif;">&nbsp;</td>
					<td valign="top" width="25" style="font-size: 1px; font-family: Georgia, Times, serif; padding: 20px 0 25px;">
								
					<td width="505" valign="top" align="left" style="font-family: Georgia, Times, serif;" class="content">
						<table cellpadding="0" cellspacing="0" border="0"  style="color: #717171; font: normal 11px Georgia, Times, serif; margin: 0; padding: 0;" width="505">
						<tr>
							<td style="padding: 20px 0 25px;" align="left">			
								<h2 style="color:#055d90; font-weight: normal; margin: 0; padding: 0; line-height: 26px; font-size: 25px;font-family: Georgia, Times, serif; ">
								
								<?php $view['slots']->output('top4'); ?></h2>
							</td>
						</tr>
						<tr>
							<td style="padding: 0px 0 15px;"  valign="top">
							
								<?php $view['slots']->output('img1'); ?>
								
							</td>
						</tr>
						<tr>
							<td style="padding: 10px 0 25px;"  valign="top">
								<p style="color:#585858; font-weight: normal; margin: 0; padding: 0; line-height: 20px; font-size: 14px;font-family: Georgia, Times, serif; ">
									<?php $view['slots']->output('top5'); ?>
								</p>
							</td>
						</tr>
						<tr>
					        <td align="left" style="height:16px; font-size: 1px; line-height: 1px" height="16" valign="top">
							
								<div class="divider"></div>
							
							</td>
					     </tr>
						<tr>
							<td style="padding: 20px 0 20px;" align="left">			
								<h2 style="color:#055d90; font-weight: normal; margin: 0; padding: 0; line-height: 26px; font-size: 25px;font-family: Georgia, Times, serif; "><?php $view['slots']->output('top6'); ?></h2>
							</td>
						</tr>
						<tr>
							<td style="padding: 0px 0 25px;"  valign="top">
								<p style="color:#585858; font-weight: normal; margin: 0; padding: 0; line-height: 20px; font-size: 14px;font-family: Georgia, Times, serif; ">
									<?php $view['slots']->output('top7'); ?>	
								</p>
							</td>
						</tr>
							<tr>
						        <td align="left" style="height:16px; font-size: 1px; line-height: 1px" height="16" valign="top">
								
									<div class="divider_wide"></div>
								
								</td>
						     </tr>
						</table>	
					</td>
				    </tr>
			       <tr>
				      <td align="left" colspan="3" style="font-size: 1px; padding: 10px 0">
						<table cellpadding="0" cellspacing="0" border="0"  style="color: #717171; font: normal 11px Georgia, Times, serif; margin: 0; padding: 0;" width="600">
							<tr>
								<td width="420" align="right" valign="top" style="padding: 0px 0 10px;">
								
									<div><?php $view['slots']->output('bottom1'); ?></div>
				
								</td>
								<td style="padding: 10px 0 20px; font-family: Georgia, Times, serif; " width="180" valign="top">
									<h3 style=" margin: 0; padding: 0; line-height: 20px;">
									</h3>	
									</p>
								</td>
						</table>
					  </td>
				  </tr>
			      <tr>
				      <td align="right" colspan="3" style="height: 16px; font-size: 1px;" height="16">

					  </td>
				  </tr>
				
				</table><!-- body -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: Georgia, Times, serif; line-height: 10px;" class="footer">
				  <tr>
					
			        <td style="font-size: 11px; color:#616161; margin: 0; font-family: Georgia, Times, serif; " valign="top" align="left">
					
						<p>
							<?php $view['slots']->output('bottom5'); ?>
						</p>
			        </td>
				 </tr>
				</table><!-- footer-->
		  	</td>
		</tr>
    </table>

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>