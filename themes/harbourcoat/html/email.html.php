<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/harbourcoat/css/style.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body class="body" style="text-align: left; padding: 0; margin: 0;">
	<table cellspacing="0" border="0" align="center" style="" width="100%" cellpadding="0">
		<tr>
			<td align="center" valign="top" style="font-family: Helvetica, Arial, sans-serif;">

				<table cellspacing="0" border="0" cellpadding="0" width="600">
					<tr>
						<td align="left" style="padding: 0 0 16px; font-family: Helvetica, Arial, sans-serif;">

							<table class="unsubscribe" cellspacing="0" border="0" align="left" cellpadding="0" width="600">
								<tr>
									<td align="center" style="padding: 12px 0; font-family: Helvetica, Arial, sans-serif;">
										<p style="padding: 0; font-size: 12px; line-height: 16px; color: #666; margin: 0;">
										<?php $view['slots']->output('top1'); ?>
										
										</p>
									</td>
								</tr>
							</table>  <!-- unsubscribe -->

						</td>
					</tr>
					<tr>
						<td align="left" style="padding: 0 0 20px; font-family: Helvetica, Arial, sans-serif;">
							<table class="header" cellspacing="0" border="0" align="left" cellpadding="0" width="600">
								<tr>
									<td style="font-family: Helvetica, Arial, sans-serif;">
									
									
									<div class="bgheadertop"></div>
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" style="font-family: Helvetica, Arial, sans-serif;">
										<table cellspacing="0" border="0" align="left" bgcolor="#2d807c" style="">
											<tr>
												<td valign="top" bgcolor="#2d807c" style="padding: 10px; font-family: Helvetica, Arial, sans-serif;">
													<h1 style="font-weight: bold; padding: 0; font-size: 60px; line-height: 76px; color: #ffe3a9; margin: 10px 0 0;"><?php $view['slots']->output('top2'); ?></h1>
												</td>
											</tr>
											<tr>
												<td valign="top" bgcolor="#2d807c" style="padding: 10px; font-family: Helvetica, Arial, sans-serif;">
													<table cellspacing="0" border="0" align="left" bgcolor="#2d807c" style="" cellpadding="0">
														<tr>
															<td valign="top" bgcolor="#2d807c" style="padding: 0 0 10px; font-family: Helvetica, Arial, sans-serif;" width="204">
																<p style="padding: 0; font-size: 13px; line-height: 16px; color: #f9e9c1; margin: 0 0 10px;">
																
															
																
																</p>

																<?php $view['slots']->output('top3'); ?>

															</td>
															<td bgcolor="#2d807c" style="font-family: Helvetica, Arial, sans-serif;" width="38">&nbsp;</td>
															
															<td valign="top" bgcolor="#2d807c" style="font-family: Helvetica, Arial, sans-serif;">
															
																<h3 style="font-weight: normal; padding: 0; font-size: 16px; line-height: 20px; color: #f9e9c1; margin: 6px 0 0;">
																
																<?php $view['slots']->output('top4'); ?>
																
																</h3>
																
															</td>
														</tr>
													</table>  
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>  <!-- header -->
						</td>
					</tr>
					<tr>
						<td align="left" style="padding: 0 0 20px; font-family: Helvetica, Arial, sans-serif;">
							<table class="content-item" cellspacing="0" border="0" align="left" bgcolor="#fff" style="background: #fff; border: 1px solid #bddacb;" cellpadding="0" width="600">
								<tr>
									<td bgcolor="#fff" style="padding: 10px 10px 0; font-family: Helvetica, Arial, sans-serif; background: #fff;" colspan="2">
										<h2 style="padding: 6px 10px; background: #9ebbb0; font-weight: bold; font-size: 14px; line-height: 16px; margin: 0; color: #fdfdfd;">
										
											<?php $view['slots']->output('top5'); ?>
										
										</h2>
									</td>
								</tr>
								<tr>
									<td class="pcontent" valign="top" bgcolor="#fff" style="padding: 10px; font-family: Helvetica, Arial, sans-serif; background: #fff;">
										<p class="pcontent" style="padding: 0; font-size: 13px; line-height: 16px; color: #2d817d;">
											<?php $view['slots']->output('top6'); ?>
										</p>
										
									</td>
									<td valign="top" bgcolor="#fff" style="padding: 10px; font-family: Helvetica, Arial, sans-serif; background: #fff;">
										
									</td>
								</tr>
							</table>  
						</td>
					</tr>	
					<tr>
						<td align="left" style="padding: 0 0 10px; font-family: Helvetica, Arial, sans-serif;">
							<table class="footer" cellspacing="0" border="0" align="left" bgcolor="#5b9b83" style="background: #5b9b83 url(images/bg_footer.png);" cellpadding="10" width="600">
								<tr>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;" width="10">&nbsp;</td>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;">
										<h4 style="padding: 0; font-size: 13px; line-height: 14px; color: #fff; margin: 0;"><?php $view['slots']->output('bottom1'); ?></h4>
									</td>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;" width="5">&nbsp;</td>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;">
										<h4 style="padding: 0; font-size: 13px; line-height: 14px; color: #fff; margin: 0;"><?php $view['slots']->output('bottom2'); ?></h4>
									</td>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;" width="5">&nbsp;</td>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;">
										<h4 style="padding: 0; font-size: 13px; line-height: 14px; color: #fff; margin: 0;"><?php $view['slots']->output('bottom3'); ?></h4>
									</td>
									<td bgcolor="#1b4a15" style="font-family: Helvetica, Arial, sans-serif; background: #1b4a15;" width="10">&nbsp;</td>
								</tr>
								<tr>
									<td bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">&nbsp;</td>
									<td class="top" valign="top" bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">
										<p style="padding: 0; font-size: 13px; line-height: 16px; color: #ffe3a9; margin: 0 0 14px;">
										
										<?php $view['slots']->output('bottom4'); ?>
										</p>
										
									</td>
									<td bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">&nbsp;</td>
									<td class="top" valign="top" bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">
										<p style="padding: 0; font-size: 13px; line-height: 16px; color: #ffe3a9; margin: 0 0 14px;">

										<?php $view['slots']->output('bottom5'); ?>
										
										</p>
										
									</td>
									<td bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">&nbsp;</td>
									<td class="top" valign="top" bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">
										<p style="padding: 0; font-size: 13px; line-height: 16px; color: #ffe3a9; margin: 0 0 14px;">
											<?php $view['slots']->output('bottom6'); ?>
										</p>
									</td>
									<td bgcolor="#5b9b83" style="font-family: Helvetica, Arial, sans-serif; background: #5b9b83 url(images/bg_footer.png);">&nbsp;</td>
								</tr>
							</table>      		  	  
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
 

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>