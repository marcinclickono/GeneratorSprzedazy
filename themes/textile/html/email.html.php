<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/textile/css/textile.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body style="margin: 0; padding: 0;" class="body">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td valign="top" align="center" id="top" class="outer">
			<table class="texttopbottom" cellpadding="0" cellspacing="0" align="center" width="600">
				<tr>
					<td valign="top">
					
					<div class="div4"></div>
					
					
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" colspan="3" style="font-family: Verdana, Arial, sans-serif; color: #8d9ca4;font-size: 10px;margin: 0;">
						<p><?php $view['slots']->output('top1'); ?></p>
						
					</td>
				</tr>
				<tr>
					<td style="background: url(images/bg_top.png) no-repeat;">
						<div class="blank2"></div>
				
					
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" align="center" width="600" style="background: url(images/bg_email_singlecol.png) repeat-y;">
				<tr>
					<td rowspan="4" valign="top">
					
						<div class="div3"></div>
				
						
						<div class="blank"></div>
						
					</td>
					<td rowspan="4" valign="top">
					
						<div class="header_strip"></div>
					
					</td>
					<td rowspan="3" valign="top">
									
						<div class="headerimg"></div>
					
					</td>
					<td bgcolor="#fbfdfd" colspan="2" width="100" height="26">
					
						
						<div class="blank3"></div>
						
					</td>
					<td rowspan="4" valign="top">
					
					
						<div class="header_r"></div>
						
						
					</td>
					<td rowspan="4" valign="top">
					
					<div class="div3"></div>
					
					
					</td>
				</tr>
				<tr>
					<td valign="top">
					
						
						<div class="headermbsg"></div>
					</td>
					<td valign="top" width="84" align="left" class="date" height="79">
						<p class="month"><currentmonthname></p>
						<p class="year"><currentyear></p>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					
						
						<div class="header_m"></div>
						
					</td>
				</tr>
				<tr>
					<td class="bgh1" valign="bottom" colspan="3" height="65" align="left">
						<h1>
                        	<?php $view['slots']->output('top2'); ?>
                        </h1>
					</td>
				</tr>
				

				<tr>
					<td colspan="7" align="left" valign="top">
						<table cellpadding="0" cellspacing="0" width="600" align="left">
							<tr>
								<td colspan="2">
								
								<div class="singlecol"></div>
						
								
								</td>
							</tr>
							<tr>
								<td valign="top" width="600">
									<repeater>
									<table cellpadding="0" cellspacing="0" width="600" class="whitecol">
										<tr>
											<td valign="top" width="15" rowspan="3">
											
											
											
											<div class="rightsinglecol"></div>
											
											</td>
											<td bgcolor="#eaf4f7" valign="top" width="21" rowspan="3">
											

												<div class="tblue"></div>
											</td>
											<td bgcolor="#eaf4f7">
											
											<div class="div2"></div>
											
											</td>
											<td bgcolor="#eaf4f7" rowspan="4">
											
											<div class="div2"></div>
											
											</td>
											<td rowspan="4">
											<div class="div3"></div>
											
											</td>
										</tr>
										<tr>											
											<td bgcolor="#eaf4f7" valign="top" width="529">
											
											<h2 id="titleh2">
											
											<?php $view['slots']->output('top3'); ?>
											</h2>
											
											</td>
										</tr>
										<tr>
											<td bgcolor="#eaf4f7" valign="top" class="border">
												<p><?php $view['slots']->output('top4'); ?></p>
												<ul></ul>
																							
											
											</td>
										</tr>
										<tr>
											<td valign="top" width="15">&nbsp;</td>
											<td bgcolor="#eaf4f7" colspan="2" valign="top" align="right" class="backtotop">
											
											<a href="#top">Back to top</a></td>
										</tr>
									</table>
									</repeater>
								</td>
							</tr>
							<tr>
								<td colspan="2">
								
								
								<div class="topbot"></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table class="texttopbottom" cellpadding="0" cellspacing="0" align="center" width="600">
				<tr>
					<td colspan="3" style="background: url(images/bg_bottom.png) no-repeat;">
					
					<div class="div1" width="1" height="12" alt="" /></td>
					
				</tr>
				<tr>
					<td rowspan="2">
					<div class="div3"></div>
					
					
					</td>
					<td valign="top" align="center" style="font-family: Verdana, Arial, sans-serif; color: #8d9ca4;font-size: 10px;margin: 0;">
                    	<p>
                        	<?php $view['slots']->output('bottom1'); ?>
                        
                        </p>
                        <p>
                           <?php $view['slots']->output('bottom2'); ?>  
                        </p>
                    </td>
					<td rowspan="2">
					<div class="div3"></div>
					
					
					</td>
				</tr>
				<tr>
					<td>
				
					<div class="blanko"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>