<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

$config = array(
    "name"        => "Classic",
    "features"    => array(
		"email"
    ),
    "slots"       => array(
        "page" => array(
            "top1",
            "top2",
            "top3",
			"top4",
			"top5",
			"top6",
			"top7",
			"top8",
			"top9",
			"top10",
			"top11",
			"top12",
            "bottom1",
            "bottom2",
            "bottom3",
			"bottom4",
			"bottom5",
			"bottom6",
			"bottom7",
			"bottom8",
			"bottom9",
			"bottom10",
			"bottom11",
			"bottom12",
			"bottom13"
        ),
        "email" => array(
            "top1",
            "top2",
            "top3",
			"top4",
			"top5",
			"top6",
			"top7",
			"top8",
			"top9",
			"top10",
			"top11",
			"top12",
            "bottom1",
            "bottom2",
            "bottom3",
			"bottom4",
			"bottom5",
			"bottom6",
			"bottom7",
			"bottom8",
			"bottom9",
			"bottom10",
			"bottom11",
			"content0",
			"content1",
			"content2",
			"content3",
			"content4",
			"img0",
			"img1",
			"img2",
			"img3",
			"img4",
			"img5",
			"img6",
			"img7",
			"img8",
			"img9",
			"img10",
			"img11",
			"ribbon1",
			"ribbon2"
        )
    )
);

return $config;