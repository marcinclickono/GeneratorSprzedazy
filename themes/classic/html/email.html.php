<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/classic/css/style.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f7f2e4;" bgcolor="#f7f2e4" leftmargin="0">
<!--100% body table-->
<table class="tabel" cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f7f2e4">
  <tr>
    <td>
	<!--top links-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="middle" align="center" height="45">
		<p style="font-size: 14px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #b0a08b; margin: 0px;">
		
			<?php $view['slots']->output('top1'); ?>
		</p>
		</td>
	</tr>
</table>
   <!--header-->
   <table class="tabela">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top" width="173">
		<!--ribbon-->
		<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="120" width="45"></td>
		<td class="ribbon">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom" align="center" height="35">
		<p class="ribbonp" style="font-size: 14px; font-family: Georgia, 'Times New Roman', Times, serif; color: #ffffff; margin-top: -20px; margin-bottom: 0px;">
		
		<?php $view['slots']->output('ribbon1'); ?>
		
		</p>
		</td>
	</tr>
	
</table>
		</td>
	</tr>
	</table><!--ribbon-->
		</td>
		<td valign="middle" width="493"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="60"></td>
			</tr>
			<tr>
				<td>
				<h1 style="color: #333; margin-top: 0px; margin-bottom: 0px; font-weight: normal; font-size: 48px; font-family: Georgia, 'Times New Roman', Times, serif">
				
				<?php $view['slots']->output('top2'); ?>
				
				</h1>
				</td>
			</tr>
			<tr>
				<td height="40">
				</td>
			</tr>
		</table>
		<!--date-->
		<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="pasek">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td height="5"></td></tr></table>
        <p style="font-size: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #ffffff; margin-top: 0px; margin-bottom: 0px;"><currentmonthname> <currentyear></p>
		
		</td>
	</tr>
	</table><!--/date-->
		</td>
		<td width="18"></td>
	</tr>
</table>
	</td>
    </tr>
</table><!--/header-->
    <!--email container-->
    <table bgcolor="#fffdf9" cellspacing="0" border="0" align="center" cellpadding="30" width="684">
  <tr>
    <td>
    <!--email content-->
    <table cellspacing="0" border="0" id="email-content" cellpadding="0" width="624">
  <tr>
    <td>
    <!--section 1-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td valign="top" align="center">
	
	<?php $view['slots']->output('img1'); ?>
    
	<!--line break-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom" height="50">
		
		<div class="linebreak"></div>
		
		
		</td>
	</tr>
</table><!--/line break-->
	<h1 style="font-size: 36px; font-weight: normal; color: #333333; font-family: Georgia, 'Times New Roman', Times, serif; margin-top: 0px; margin-bottom: 20px;">
	
	<?php $view['slots']->output('top3'); ?>
	
	</h1>
    <p style="font-size: 16px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	<?php $view['slots']->output('top4'); ?>
	
	</p>
    </td>
  </tr>
</table><!--/section 1-->
<!--line break-->
  <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td height="72">
	
		<div class="linebreak2"></div>
	
    </td>
  </tr>
</table><!--/line break-->
    <!--section 2-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td>
    <table cellspacing="0" border="0" cellpadding="8" width="100%" style="margin-top: 10px;">
  <tr>
    <td valign="top"><p style="font-size: 17px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	<?php $view['slots']->output('img2'); ?>
	
	
	</p>
	
    <p style="color: #333333; font-size: 18px; font-family: Georgia, 'Times New Roman', Times, serif; margin: 12px 0px; font-weight: bold;">
	<?php $view['slots']->output('top5'); ?>
	</p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	<?php $view['slots']->output('top6'); ?>
	
	</p>
    </td>
    
    <td valign="top"><p style="font-size: 17px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	
	
	<?php $view['slots']->output('img3'); ?>
	
	</p>
     <p style="font-size: 18px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333333; margin: 12px 0px; font-weight: bold;">
	 
	 <?php $view['slots']->output('top7'); ?>
	 
	 </p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	<?php $view['slots']->output('top8'); ?>
	
	</p>
    </td>
    
    <td valign="top"><p style="font-size: 17px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	<?php $view['slots']->output('img4'); ?>
	
	
	</p>
     <p style="font-size: 18px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333333; margin: 12px 0px; font-weight: bold;">

	 <?php $view['slots']->output('top9'); ?>
	 
	 </p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	 <?php $view['slots']->output('top10'); ?>
	
	</p>
    </td>
    
    <td valign="top"><p style="font-size: 17px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	
	<?php $view['slots']->output('img5'); ?>
	
	</p>
     <p style="font-size: 18px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333333; margin: 12px 0px; font-weight: bold;">
	 <?php $view['slots']->output('top11'); ?>
	 
	 </p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	<?php $view['slots']->output('top12'); ?>
	
	</p>
    </td>
  </tr>
</table>
    </td>
  </tr>
</table><!--/section 2-->
    <!--section 3-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td>
<!--line break-->
  <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td height="72">
	
		<div class="linebreak2"></div>
	
    </td>
  </tr>
</table><!--/line break-->
<table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td valign="top" width="378">
    <h1 style="font-size: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333333; margin-top: 0px; margin-bottom: 12px;">
	
	<?php $view['slots']->output('bottom1'); ?>
	
	</h1>
    <p style="font-size: 16px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
	<?php $view['slots']->output('bottom2'); ?>
	
	</p>
    </td>
    <td valign="top" width="246">
	
	<?php $view['slots']->output('bottom3'); ?>
	
	</td>
  </tr>
</table>
	<!--line break-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom" height="50">
		
			<div class="linebreak"></div>

		</td>
	</tr>
</table><!--/line break-->
<table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td valign="top" width="378">
    <h1 style="font-size: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333333; margin-top: 0px; margin-bottom: 12px;">
	
	<?php $view['slots']->output('bottom4'); ?>
	
	</h1>
    <p style="font-size: 16px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	<?php $view['slots']->output('bottom5'); ?>
	
	</p>
    </td>
    <td valign="top" width="246">

	<?php $view['slots']->output('img6'); ?>
	
	</td>
  </tr>
</table>
	<!--line break-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom" height="50">
		
		<div class="linebreak"></div>

		
		</td>
	</tr>
</table><!--/line break-->
<table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td valign="top" width="378">
    <h1 style="font-size: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333333; margin-top: 0px; margin-bottom: 12px;">
	
	<?php $view['slots']->output('bottom6'); ?>
	
	</h1>
	
	<p style="font-size: 16px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
	
		<?php $view['slots']->output('bottom11'); ?>
		
	</p>
    </td>
    <td valign="top" width="246">
	
	<?php $view['slots']->output('img7'); ?>
	
	</td>
  </tr>
</table>
<!--line break-->
  <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td height="72">
	
		<div class="linebreak2"></div>

    </td>
  </tr>
</table><!--/line break-->
    </td>
  </tr>
</table><!--/section 3-->
    </td>
  </tr>
</table><!--/email content-->
    </td>
  </tr>
</table><!--/email container-->
<!--footer-->
    <table width="680" border="0" align="center" cellpadding="30" cellspacing="0">
  <tr>
    <td valign="top">
    <p style="font-size: 14px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #b0a08b; margin: 0px;">
	
	<?php $view['slots']->output('bottom7'); ?>
	</p>
    </td>
    <td valign="top">
	<p style="font-size: 14px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #b0a08b; margin: 0px;">
	
	<?php $view['slots']->output('bottom8'); ?>
	
	</p></td>
  </tr>
  <tr>
  	<td height="30"></td>
  	<td height="30"></td>
  	</tr>
	</table><!--/footer-->
    </td>
  </tr>
</table><!--/100% body table-->
</body>

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>