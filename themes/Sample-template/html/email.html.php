<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body style="background: #969696;">
  
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#e4e4e4">
<tr>
	<td bgcolor="#e4e4e4" width="100%">

	<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="table">
	<tr>
		<td width="600" class="cell">
		
	   	<table width="600" cellpadding="0" cellspacing="0" border="0" class="table">
		<tr>
			<td width="250" bgcolor="#e4e4e4" class="logocell">
			<div class="spacer"></div>
		
			
			<br class="hide">
			
			<div>
				<?php $view['slots']->output('img11'); ?>
			</div>
			
			<br>
			<div class="spacer2"></div>
			
			
			<br class="hide"></td>
			<td align="right" width="350" class="hide" style="color:#a6a6a6;font-size:12px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;text-shadow: 0 1px 0 #ffffff;" valign="top" bgcolor="#e4e4e4">
			
			<div class="spacer3"></div>
			
			
			<br><span>WIDGET&nbsp;</span><strong><span style="text-transform:uppercase;"> <currentmonthname> <currentyear></span></strong> <span>NEWSLETTER&nbsp;</span></td>
		</tr>
		</table>
	
		<div>
		<img border="0" label="Hero image" editable="true" width="0" height="0" id="screenshot">
			<?php $view['slots']->output('img0'); ?>
		</div>
	
		<table width="600" cellpadding="25" cellspacing="0" border="0" class="promotable">
		<tr>
			<td bgcolor="#456265" width="600" class="promocell">                      
			 
				<multiline label="Main feature intro"><p><?php $view['slots']->output('top1'); ?></p></multiline>
			
			</td>
		</tr>
		</table>
	
		<div class="spacer4"></div>
		
		<br>
	
		<repeater>
			<layout label="New feature">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td bgcolor="#85bdad" nowrap>
				
				<img border="0" width="5" height="1">
				</td>
				<td width="100%" bgcolor="#ffffff">
			
					<table width="100%" cellpadding="20" cellspacing="0" border="0">
					<tr>
						<td bgcolor="#ffffff" class="contentblock">

							<h4 class="secondary"><strong><singleline label="Title"><?php $view['slots']->output('top2'); ?></singleline></strong></h4>
							<multiline label="Description"><p><?php $view['slots']->output('top3'); ?></p></multiline>

						</td>
					</tr>
					</table>
			
				</td>
			</tr>
			</table>  
			<div class="spacer4"></div>
		
			</layout>
			<layout label="Article, tip or resource">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td bgcolor="#ef3101" nowrap>
				
				<img border="0" width="5" height="1">
				
				</td>
				<td width="100%" bgcolor="#ffffff">
			
					<table width="100%" cellpadding="20" cellspacing="0" border="0">
					<tr>
						<td bgcolor="#ffffff" class="contentblock">

							<h4 class="secondary"><strong><singleline label="Title"><?php $view['slots']->output('top4'); ?></singleline></strong></h4>
							<multiline label="Description"><p><?php $view['slots']->output('top5'); ?></p></multiline>

						</td>
					</tr>
					</table>
			
				</td>
			</tr>
			</table>  
			<div class="spacer5"></div>
			<br>
			</layout>
			<layout label="Gallery highlights">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td bgcolor="#832701" nowrap><img border="0" width="5" height="1"></td>
				<td width="100%" bgcolor="#ffffff">

					<table width="100%" cellpadding="20" cellspacing="0" border="0">
					<tr>
						<td bgcolor="#ffffff" class="contentblock">

							<h4 class="secondary"><strong><singleline label="Gallery title"><?php $view['slots']->output('bottom1'); ?></singleline></strong></h4>
							<multiline label="Gallery description"><p><?php $view['slots']->output('bottom2'); ?></p></multiline>

						</td>
					</tr>
					</table>

				</td>
			</tr>
			</table>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td bgcolor="#832701" nowrap>
				
				<img border="0" width="5" height="1">
				
				</td>
				<td bgcolor="#ffffff" nowrap>
				
				<img border="0" width="5" height="1">
				
				</td> 
				<td width="100%" bgcolor="#ffffff">

					<table cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td><src border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 1"><?php $view['slots']->output('img1'); ?>
						</td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 2"><?php $view['slots']->output('img2'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 3"><?php $view['slots']->output('img3'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 4"><?php $view['slots']->output('img4'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 5"><?php $view['slots']->output('img5'); ?></td>
					</tr>
					<tr>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 6"><?php $view['slots']->output('img6'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 7"><?php $view['slots']->output('img7'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 8"><?php $view['slots']->output('img8'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 9"><?php $view['slots']->output('img9'); ?></td>
						<td><img border="0" width="0" height="0" editable="true" class="galleryimage" label="Image 10"><?php $view['slots']->output('img10'); ?></td> 
					</tr>
					</table>

					<div class="spacer5"></div>
					<br>

				</td>
			</tr>
			</table>  
			<div class="spacer4"></div>
			<br>
			</layout>
			<layout label="Pull quote">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td bgcolor="#85bdad" nowrap>
							<div class="spacer5"></div>
							<img border="0" width="5" height="1">
							
							</td>
							<td width="100%" bgcolor="#ffffff">
						
								<table width="100%" cellpadding="20" cellspacing="0" border="0">
								<tr>
									<td bgcolor="#ffffff" class="contentblock">
			
										<h4 class="secondary"><strong><singleline label="Pull quote"><?php $view['slots']->output('bottom3'); ?></singleline></strong></h4>
			
									</td>
								</tr>
								</table>
						
							</td>
						</tr>
						</table>  
						<div class="spacer4"></div>
						<br>
						</layout>
		</repeater>           
		
		</td>
	</tr>
	</table>

	<img border="0" width="1" height="25" class="divider"><br>

	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f2f2f2">
	<tr>
		<td>
		
			<img border="0" width="1" height="30"><br>
		
			<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="table">
			<tr>
				<td width="600" nowrap bgcolor="#f2f2f2" class="cell">
				
					<table width="600" cellpadding="0" cellspacing="0" border="0" class="table">
					<tr>
						<td width="380" valign="top" class="footershow">
						
							<div class="spacer8"></div>
							<br>  
						
							<p style="color:#a6a6a6;font-size:12px;font-family:Helvetica,Arial,sans-serif;margin-top:0;margin-bottom:15px;padding-top:0;padding-bottom:0;line-height:18px;" class="reminder"><?php $view['slots']->output('bottom4'); ?></p>
							<p style="color:#c9c9c9;font-size:12px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;"><preferences style="color:#3ca7dd;text-decoration:none;"><strong><?php $view['slots']->output('bottom5'); ?></strong></preferences>&nbsp;&nbsp;&nbsp;&nbsp;<unsubscribe style="color:#3ca7dd;text-decoration:none;"><strong><?php $view['slots']->output('bottom6'); ?></strong></unsubscribe></p>
						
						</td>
						<td align="right" width="220" style="color:#a6a6a6;font-size:12px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;text-shadow: 0 1px 0 #ffffff;" valign="top" class="hide">
							<table cellpadding="0" cellspacing="0" border="0">
							<tr>
							
							</tr>
							</table>
							
							<div class="spacer10"></div>
						
							
							<br><p style="color:#b3b3b3;font-size:11px;line-height:15px;font-family:Helvetica,Arial,sans-serif;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;font-weight:bold;"><?php $view['slots']->output('bottom7'); ?></p><p style="color:#b3b3b3;font-size:11px;line-height:15px;font-family:Helvetica,Arial,sans-serif;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;font-weight:normal;"><?php $view['slots']->output('bottom8'); ?></p>
						</td>
					</tr>
					</table>
				
				</td>
			</tr>	
	   		</table>

			<img border="0" width="1" height="25"><br>
		
	   </td>
	</tr>
	</table>
	
	</td>
</tr>
</table>
<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>