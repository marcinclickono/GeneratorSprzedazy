<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/elegant/css/elegant.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body style="margin: 0; padding: 0;" class="body">

<table cellspacing="0" border="0">
	
	<tr>
		
		<td valign="top">

			<table cellspacing="0" border="0" align="center" style="background: #fff; border-right: 1px solid #ccc; border-left: 1px solid #ccc;" cellpadding="0" width="600">
				<tr>
					<td valign="top">
						<!-- header -->
						<table cellspacing="0" border="0" height="157" cellpadding="0" width="600">
							<tr>
								
								<td class="header-text" height="50" valign="top" style="color: #999; font-family: Verdana; font-size: 10px; text-transform: uppercase; padding: 0 20px;" width="540" colspan="2">
									<br /><br /><?php $view['slots']->output('top1'); ?>
								</td>
								
							</tr>
							<tr>
								<td class="main-title" height="13" valign="top" style="padding: 0 20px; font-size: 25px; font-family: Georgia; font-style: italic;" width="600" colspan="2">
									<?php $view['slots']->output('top2'); ?>
								</td>
							</tr>
							<tr>
								<td height="20" valign="top" width="600" colspan="2">
									<div class="breakerimage"></div>
								
								</td>
							</tr>
							<tr>
								<td class="header-bar" valign="top" style="color: #999; font-family: Verdana; font-size: 10px; text-transform: uppercase; padding: 0 20px; height: 15px;" width="400" height="15">
									<?php $view['slots']->output('top3'); ?>
								</td>
								<td class="header-bar" valign="top" style="color: #999; font-family: Verdana; font-size: 10px; text-transform: uppercase; padding: 0 20px; height: 15px; text-align: right;" width="200">
									<currentdayname> <currentday> <currentmonthname> <currentyear>
								</td>
								<tr>
									<td height="20" valign="top" width="600" colspan="2">
										
										
										<div class="breakerimage"></div>
									</td>
								</tr>
						</table>
						<!-- / header -->
					</td>
				</tr>
				<tr>
					<td>
						<!-- content -->
						<table cellspacing="0" border="0" cellpadding="0" width="600">
							<tr>
								<td class="article-title" height="45" valign="top" style="padding: 0 20px; font-family: Georgia; font-size: 20px; font-weight: bold;" width="600" colspan="2">
									<?php $view['slots']->output('top4'); ?>
								</td>
							</tr>
							<tr>
								<td class="content-copy" valign="top" style="padding: 0 20px; color: #000; font-size: 14px; font-family: Georgia; line-height: 20px;" colspan="2">
									<?php $view['slots']->output('top5'); ?> 
								</td>
							</tr>
							<tr>
								<td class="image" valign="top" style="padding: 10px 20px;" colspan="2">
									
									<?php $view['slots']->output('bottom1'); ?> 
									
								</td>
							</tr>
							<tr>
								<td class="article-title" height="39" valign="top" style="padding: 0 20px; font-family: Georgia; font-size: 20px; font-weight: bold;" width="600" colspan="2">
									<?php $view['slots']->output('bottom2'); ?> 
								</td>
							</tr>
							<tr>
								<td class="content-copy" valign="top" style="padding: 0 20px; color: #000; font-size: 14px; font-family: Georgia; line-height: 20px;" colspan="2">
									<?php $view['slots']->output('bottom3'); ?> 
								</td>
							</tr>
							<tr>
								<td class="article-title" height="39" valign="top" style="padding: 0 20px; font-family: Georgia; font-size: 20px; font-weight: bold;" width="600" colspan="2">
									<?php $view['slots']->output('bottom4'); ?> 
								</td>
							</tr>
							<tr>
								<td>
									<table cellspacing="0" border="0" cellpadding="0">
										<tr>
											<td class="floated-copy" valign="top" style="padding: 0 20px 20px 20px; color: #000; font-size: 14px; font-family: Georgia; line-height: 20px;" width="600">
												
												<?php $view['slots']->output('bottom5'); ?> 
												
												<?php $view['slots']->output('bottom6'); ?> 
											</td>
										</tr>
									</table>
								</td>
							</tr>

						</table>
						<!--  / content -->
					</td>
				</tr>
				<tr>
					<td valign="top" width="600">
						<!-- footer -->
						<table cellspacing="0" border="0" height="202" cellpadding="0" width="600">
							<tr>
								<td height="20" valign="top" width="600" colspan="2">
								
									
									<div class="breakerimage"></div>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<table cellspacing="0" border="0" width="600" cellpadding="0">
										<tr>
											<td class="unsubscribe" valign="top" style="padding: 20px; color: #999; font-size: 14px; font-family: Georgia; line-height: 20px;" width="305">
												<?php $view['slots']->output('bottom7'); ?> 
											</td>
											<td valign="top" width="255">
												<table cellspacing="0" width="255" cellpadding="0">
													<tr>
														<td valign="top">
															<!--button-->
															<table cellspacing="0" border="0" cellpadding="10" width="210">
															<tr>
																<td><br /></td>
															</tr>
															<tr>
																
																<?php $view['slots']->output('bottom8'); ?> 
																
															</tr>
															</table><!--/button-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" colspan="2">
								
									<div class="breakerimage"></div>	
									
								</td>
							</tr>
							<tr>
								<td class="copyright" height="100" align="center" valign="top" style="padding: 0 20px; color: #999; font-family: Verdana; font-size: 10px; text-transform: uppercase; line-height: 20px;" width="600" colspan="2">
										
										<?php $view['slots']->output('bottom9'); ?>
										
								</td>
							</tr>
						</table>
						<!-- / end footer -->
					</td>
				</tr>
			</table>
			
		</td>
	
	</tr>
	
</table>

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>