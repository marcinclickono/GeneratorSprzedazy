<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/launch/css/style.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:Helvetica, Arial,serif;'>

    <!-- =============================== Header ====================================== -->

    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
          <tr>
            <td class='movableContentContainer'  >


              <div class='movableContent'>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                   <tr><td height='25'  colspan='3'></td></tr>

                    <tr>
                      <td valign='top'  colspan='3'>
                        <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                          <tr>
                            <td align='left' valign='middle' >
                              <div class="contentEditableContainer contentImageEditable">
                                <div class="contentEditable" >
								
								  <?php $view['slots']->output('logo'); ?>
								  
                                </div>
                              </div>
                            </td>

                            <td align='right' valign='top' >
                              <div class="contentEditableContainer contentTextEditable" style='display:inline-block;'>
                                <div class="contentEditable" >
                                  <a target='_blank' href="[SHOWEMAIL]" style='color:#A8B0B6;font-size:13px;text-decoration:none;'>
									<?php $view['slots']->output('top1'); ?>
								  </a>
                                </div>
                              </div>
                            </td>
                            <td width='18' align='right' valign='top'>
                              <div class="contentEditableContainer contentImageEditable" style='display:inline-block;'>
                                <div class="contentEditable" >
                                 
									<?php $view['slots']->output('topleft'); ?>
								 
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                </table>
              </div>

              <div class='movableContent'>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr><td height='25'  ></td></tr>

                        <tr>
                          <td height='290'  class='bgItem'>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>

                              <tr><td height='50' ></td></tr>

                              <tr>
                                <td width='350' >
                                  <div class="contentEditableContainer contentImageEditable">
                                    <div class="contentEditable" >
									
									  <?php $view['slots']->output('img0'); ?>
									  
                                    </div>
                                  </div>
                                </td>
                                <td width='250' valign='top'>
                                  <table width="250" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                    <tr><td colspan='3' height='10'></td></tr>

                                    <tr>
                                      <td width='10'></td>
                                      <td width='230' valign='top'>
                                        <table class="table" width="230" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                          <tr>
                                            <td valign='top'>
                                              <div class="contentEditableContainer contentTextEditable">
                                                <div class="contentEditable" >
                                                  <h1 style='font-size:24px;font-weight:normal;color:#ffffff;line-height:19px;'>
												  
													<?php $view['slots']->output('top2'); ?>
												  
												  </h1>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr><td height='18'></td></tr>
                                          <tr>
                                            <td valign='top'>
                                              <div class="contentEditableContainer contentTextEditable">
                                                <div class="contentEditable" >
                                                  <p style='font-size:13px;color:#cfeafa;line-height:19px;'>
												  <?php $view['slots']->output('top3'); ?>
												  </p>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr><td height='33'></td></tr>
                                          <tr>
                                            <td>
                                              <div class="contentEditableContainer contentTextEditable">
                                                <div class="contentEditable" >
												
                                                
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr><td height='15'></td></tr>
                                        </table>
                                      </td>
                                      <td width='10'></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                              <tr><td height='25'></td></tr>
                            </table>
                          </td>
                        </tr>

                        <tr><td height='25' ></td></tr>
                </table>
              </div>


              <div class='movableContent'>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'><tr><td>
                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                  <tr><td colspan='3' height='25'></td></tr>
                  <tr>
                    <td width='50'></td>
                    <td width='500'>
                      <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                          <td align='center'>
                            <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                <h1 style='font-size:32px;'>
								
								<?php $view['slots']->output('top4'); ?>
								
                                </h1>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr><td height='20'></td></tr>
                        <tr>
                          <td align='center'>
                            <div class='contentEditableContainer contentTextEditable'>
                             <div class='contentEditable'>
                              <p ><br>
                                <?php $view['slots']->output('top5'); ?>
                              </p>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width='50'></td>
                </tr>
                <tr><td colspan='3' height='45'></td></tr>
              </table>
              </td></tr></table>
            </div>

            <div class='movableContent'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'><tr><td>
              <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" >
                <tr><td height='10'>&nbsp;</td></tr>
                <tr><td style='border-bottom:1px solid #DDDDDD'></td></tr>
                <tr><td height='10'>&nbsp;</td></tr>
              </table>
              </td></tr></table>
            </div>

            <div class='movableContent'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'><tr><td>
              <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                <tr><td colspan='3' height='10'></td></tr>
                <tr>
                  <td width='30'></td>
                  <td width='540'>
                    <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                      <tr>
                        <td align='center'>
                          <div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable'>
                              <p >
								<?php $view['slots']->output('top6'); ?>
							  </p>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr><td height='30'></td></tr>
                      <tr>
                        <td>
                          <div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable'>
                              <table width="500" border="0" cellspacing="1" cellpadding="20" align="center" valign='top'>
                                <tr>

									<?php $view['slots']->output('downloaded'); ?>
                                
                                </tr>
                              </table>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr><td height='45'></td></tr>
                      <tr>
                        <td align='center'>
                          <div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable'>
								
								<?php $view['slots']->output('grab'); ?>
                            	  
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width='30'></td>
                </tr>
                <tr><td colspan='3' height='45'></td></tr>
              </table>
              </td></tr></table>
            </div>

            <div class='movableContent'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'><tr><td>
              <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" >
                <tr><td height='10'>&nbsp;</td></tr>
                <tr><td style='border-bottom:1px solid #DDDDDD'></td></tr>
                <tr><td height='10'>&nbsp;</td></tr>
              </table>
              </td></tr></table>
            </div>

            <div class='movableContent'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'><tr><td>
              <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                <tr><td colspan='5' height='30'></td></tr>
                <tr>
                  <td>
                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                      <tr>
                        <td width='166' valign='top'>
                          <table width="166" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                            <tr>
                              <td>
                                <div class='contentEditableContainer contentImageEditable'>
                                  <div class='contentEditable'>
								  		
									<?php $view['slots']->output('img1'); ?>
									
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr><td height='22'></td></tr>
                            <tr>
                              <td align='center'>
                                <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                    <h2><?php $view['slots']->output('bottom1'); ?></h2>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr><td height='22'></td></tr>
                            <tr>
                              <td align='center'>
                                <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                    <p ><?php $view['slots']->output('bottom2'); ?></p>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width='51'></td>
                        <td width='166' valign='top'>
                          <table width="166" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                            <tr>
                              <td>
                                <div class='contentEditableContainer contentImageEditable'>
                                  <div class='contentEditable'>
								  
									<?php $view['slots']->output('img2'); ?>
									
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr><td height='22'></td></tr>
                            <tr>
                              <td align='center'>
                                <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                    <h2><?php $view['slots']->output('bottom3'); ?></h2>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr><td height='22'></td></tr>
                            <tr>
                              <td align='center'>
                                <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                    <p ><?php $view['slots']->output('bottom4'); ?>
                                    </p>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width='51'></td>
                        <td width='166' valign='top'>
                          <table width="166" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                            <tr>
                              <td>
                                <div class='contentEditableContainer contentImageEditable'>
                                  <div class='contentEditable'>
								  
									<?php $view['slots']->output('img3'); ?>
                                
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr><td height='22'></td></tr>
                            <tr>
                              <td align='center'>
                                <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                    <h2><?php $view['slots']->output('bottom5'); ?></h2>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr><td height='22'></td></tr>
                            <tr>
                              <td align='center'>
                                <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                   <p ><?php $view['slots']->output('bottom6'); ?></p>
                                 </div>
                               </div>
                             </td>
                           </tr>
                         </table>
                       </td>
                     </tr>
                   </table>
                 </td>

               </tr>
               <tr><td colspan='5' height='45'></td></tr>
             </table>
             </td></tr></table>
           </div>

           <div class='movableContent'>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                  <tr>
                    <td>
                      <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                        <tr>
                          <td>
                            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>

                              <tr>
                                <td>
                                  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" >
                                    <tr><td height='10'>&nbsp;</td></tr>
                                    <tr><td style='border-bottom:1px solid #DDDDDD'></td></tr>
                                    <tr><td height='10'>&nbsp;</td></tr>
                                  </table>
                                </td>
                              </tr>

                              <tr><td height='28'>&nbsp;</td></tr>

                              <tr>
                                <td valign='top' align='center'>
                                  <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable" >
                                      <p style='font-weight:bold;font-size:13px;line-height: 30px;'>
									  
											<?php $view['slots']->output('bottom7'); ?>
									  
									  </p>
                                    </div>
                                  </div>
                                </td>
                              </tr>

                              <tr><td height='28'>&nbsp;</td></tr>
                              
                              <tr>
                                <td valign='top' align='center'>
                                  <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable" >
                                      <p style=' font-weight:bold;font-size:13px;line-height: 30px;'>
									  <?php $view['slots']->output('bottom8'); ?></p>
                                    </div>
                                  </div>
                                  <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable" >
                                      <p style='color:#A8B0B6; font-size:13px;line-height: 15px;'>
									  <?php $view['slots']->output('bottom9'); ?></p>
                                    </div>
                                  </div>
                                  <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable" >
                                      
										<?php $view['slots']->output('bottom10'); ?>
									
                                    </div>
                                  </div>
                                </td>
                              </tr>

                              <tr><td height='28'>&nbsp;</td></tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </div>


         </td>
       </tr>
    </table>
  </td>
</tr>




</table>

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>