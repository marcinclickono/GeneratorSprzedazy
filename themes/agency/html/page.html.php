<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend(":$template:base.html.php");
$parentVariant = $page->getVariantParent();
$title         = (!empty($parentVariant)) ? $parentVariant->getTitle() : $page->getTitle();
$view['slots']->set('public', (isset($public) && $public === true) ? true : false);
$view['slots']->set('pageTitle', $title);
?>

    <div id="gora">
        <div class="header-top">
            <div class="containnavheaderer" style="height: 0px">
                <div class="navheader" style="height: 0px">
                    <div class="container">
                        <div class="navbar-header page-scroll" style="height: 0px">

                            <div style="position:relative; top: 50px; color: #fff;" class="navbar-brand page-scroll" href="#page-top" style="z-index: 99; color: #fff">
                                    <?php $view['slots']->output('top1'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <header>
            <div class="container">
                <div class="intro-text">
                    <div class="intro-lead-in"><?php $view['slots']->output('top2'); ?></div>
                    <div class="intro-heading"><?php $view['slots']->output('top3'); ?></div>
                </div>
            </div>
        </header>
    </div>
	
	<!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php $view['slots']->output('top4'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php $view['slots']->output('top5'); ?></h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading"><?php $view['slots']->output('top6'); ?></h4>
                    <p class="text-muted"><?php $view['slots']->output('top7'); ?></p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading"><?php $view['slots']->output('top8'); ?></h4>
                    <p class="text-muted"><?php $view['slots']->output('top9'); ?></p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading"><?php $view['slots']->output('top10'); ?></h4>
                    <p class="text-muted"><?php $view['slots']->output('top11'); ?></p>
                </div>
            </div>
        </div>
    </section>
	
	
	<!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <?php $view['slots']->output('top12'); ?>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                   <?php $view['slots']->output('top13'); ?>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                   <?php $view['slots']->output('top14'); ?>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <?php $view['slots']->output('top15'); ?>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <?php $view['slots']->output('top16'); ?>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                   <?php $view['slots']->output('top17'); ?>                    
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                   <?php $view['slots']->output('top18'); ?>        
                </div>
            </div>
        </div>
    </section>
	

<?php $view['slots']->output('builder'); ?>	
	
	
	
	