<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php $view['slots']->output('pageTitle', 'Generator Sprzedaży'); ?></title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" />
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/agency/css/agency.css'); ?>" type="text/css" />
	
		
		
		<script src="<?php echo $view['assets']->getUrl('themes/agency/js/jquery.js'); ?>"></script>
		<script src="<?php echo $view['assets']->getUrl('themes/agency/js/bootstrap.min.js'); ?>"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
		<script src="<?php echo $view['assets']->getUrl('themes/agency/js/classie.js'); ?>"></script>
		<script src="<?php echo $view['assets']->getUrl('themes/agency/js/cbpAnimatedHeader.js'); ?>"></script>
		<script src="<?php echo $view['assets']->getUrl('themes/agency/js/jqBootstrapValidation.js'); ?>"></script>
		<script src="<?php echo $view['assets']->getUrl('themes/agency/js/contact_me.js'); ?>"></script>
		
		
    <!-- Custom CSS -->

    <!-- Custom Fonts -->
    <link href="<?php echo $view['assets']->getUrl('themes/agency/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <?php $view['assets']->outputHeadDeclarations(); ?>
    </head>
    <body>
        <?php $view['assets']->outputScripts("bodyOpen"); ?>
        <?php $view['slots']->output('_content'); ?>
        <?php $view['assets']->outputScripts("bodyClose"); ?>
    </body>
</html>