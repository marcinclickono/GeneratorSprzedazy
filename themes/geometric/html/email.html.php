<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/geometric/css/style.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #FFFFFF;" bgcolor="#FFFFFF" leftmargin="0">
<!--100% body table-->
<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
    <tr>
        <td>
            <!--email container-->
            <table cellspacing="0" border="0" align="center" cellpadding="0" width="624">
                <tr>
                    <td>
                        <!--header-->
                        <table cellspacing="0" border="0" cellpadding="0" width="624">
                            <tr>
                                <td valign="top"> 

								<div class="spacertop"></div>
								
                                    <!--top links-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td valign="middle" width="221">
                                                <p style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; color: #333; margin: 0px;">
												<?php $view['slots']->output('top1'); ?>
												</p>
                                            </td>
                                            <td valign="top" width="285">
                                              
                                            </td>
                                            <td valign="top" width="118">
                                              
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/top links-->
                                    <!--line break-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td valign="top" width="624">
											
                                                <p>
												
												
												<div class="lbreaksecond"></div>
												</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/line break-->
                                    <!--header content-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td>
                                                <h1 style="color: #333; margin: 0px; font-weight: normal; font-size: 60px; font-family: Helvetica, Arial, sans-serif;"><?php $view['slots']->output('top2'); ?></h1>
                                                <h2 style="color: #333; margin: 0px; font-weight: normal; font-size: 30px; font-family: Helvetica, Arial, sans-serif;">//
                                                    <currentmonthname> <currentyear>
                                                </h2>
                                            </td>
                                            <td id="issue" valign="top" style="background-repeat: no-repeat; background-position: top; width: 109px; height: 109px;">
                                                <!--number-->
                                                <table width="104" align="right" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="52" height="73" valign="bottom">
														
															<?php $view['slots']->output('img8'); ?>
															
														</td>
                                                        <td width="52" valign="bottom">
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!--/number-->
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/header content-->
                                </td>
                            </tr>
                        </table>
                        <!--/header-->
                        <!--line break-->
                        <table cellspacing="0" border="0" cellpadding="0" width="624">
                            <tr>
                                <td height="50" valign="middle" width="624">
								
								
								<div class="linebreak"></div>
								</td>
                            </tr>
                        </table>
                        <!--/line break-->
                        <!--email content-->
                        <table cellspacing="0" border="0" id="email-content" cellpadding="0" width="624">
                            <tr>
                                <td>
                                    <!--section 1-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td>
                                                <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">

												<?php $view['slots']->output('img5'); ?>
												
												</p>
                                                <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
												
									
												<div class="spacerten"></div></div>
												
												</p>
                                                <h2 style="font-size: 36px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 0px;">
												
												<?php $view['slots']->output('top3'); ?>
												</h2>
                                                <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
													<?php $view['slots']->output('top4'); ?>
												</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/section 1-->
                                    <!--line break-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td height="30" valign="middle" width="624">
											
										
											<div class="linebreak"></div></div>

											</td>
                                        </tr>
                                    </table>
                                    <!--/line break-->
                                    <!--section 2-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td>
                                                <h3 style="font-size: 24px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 0px;">
												
												
												<?php $view['slots']->output('top5'); ?>
												
												</h3>
												
                               
												
												<div class="lbreak"></div>
												
                                                <table cellspacing="0" border="0" cellpadding="8" width="100%" style="margin-top: 10px;">
                                                    <tr>
                                                        <td valign="top">
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
															
															<?php $view['slots']->output('img1'); ?>
															
															</p>
                                                            <h4 style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 12px 0px;">
															
															<?php $view['slots']->output('bottom1'); ?>
															
															</h4>
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
															
															<?php $view['slots']->output('bottom2'); ?>
															
															</p>
                                                        </td>
                                                        <td valign="top">
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
														
															
															<?php $view['slots']->output('img2'); ?>
															
															</p>
                                                            <h4 style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 12px 0px;"><?php $view['slots']->output('bottom3'); ?></h4>
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;"><?php $view['slots']->output('bottom4'); ?></p>
                                                        </td>
                                                        <td valign="top">
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
															
															<?php $view['slots']->output('img3'); ?>
															
															</p>
															
															
                                                            <h4 style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 12px 0px;"><?php $view['slots']->output('bottom5'); ?></h4>
															
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;"><?php $view['slots']->output('bottom6'); ?></p>
                                                        </td>
                                                        <td valign="top">
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
															
															
															<?php $view['slots']->output('img4'); ?>
															
															</p>
                                                            <h4 style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 12px 0px;">
															
															<?php $view['slots']->output('bottom7'); ?>
															
															</h4>
                                                            <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
															
															<?php $view['slots']->output('bottom8'); ?>
															
															</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/section 2-->
                                    <!--line break-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td height="30" valign="middle" width="624">
																				
											<div class="linebreak"></div>
											
											</td>
                                        </tr>
                                    </table>
                                    <!--/line break-->
                                    <!--section 3-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td>
                                                <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;"></p>
                                                <h3 style="font-size: 24px; font-family: Helvetica, Arial, sans-serif; color: #333 !important; margin: 0px;">
												<?php $view['slots']->output('content0'); ?>
												</h3>
                                                <!--line break-->
                                                <table cellspacing="0" border="0" cellpadding="0" width="624">
                                                    <tr>
                                                        <td height="35" valign="top"> 
														
														
																<div class="lbreak"></div>

														</td>
                                                    </tr>
                                                </table>
                                                <!--/line break-->
                                                <table cellspacing="0" border="0" cellpadding="0" width="624">
                                                    <tr>
                                                        <td valign="top" width="378">
                                                           <?php $view['slots']->output('content1'); ?>
                                                        </td>
                                                        <td valign="top" width="246">
														
											
														<?php $view['slots']->output('img9'); ?>
														
														
														</td>
                                                    </tr>
                                                </table>
                                                <!--line break-->
                                                <table cellspacing="0" border="0" cellpadding="0" width="624">
                                                    <tr>
                                                        <td height="40" valign="middle"> 
														
														

														<div class="lbreak"></div>
														
														</td>
                                                    </tr>
                                                </table>
                                                <!--/line break-->
                                                <table cellspacing="0" border="0" cellpadding="0" width="624">
                                                    <tr>
                                                        <td valign="top" width="378">
                                                            <?php $view['slots']->output('content2'); ?>
                                                        </td>
                                                        <td valign="top" width="246">
														
														<?php $view['slots']->output('img10'); ?>
														
														
														
														</td>
                                                    </tr>
                                                </table>
                                                <!--line break-->
                                                <table cellspacing="0" border="0" cellpadding="0" width="624">
                                                    <tr>
                                                        <td height="40" valign="middle"> 
														
														
														<div class="lbreak"></div>
														
														</td>
                                                    </tr>
                                                </table>
                                                <!--/line break-->
                                                <table cellspacing="0" border="0" cellpadding="0" width="624">
                                                    <tr>
                                                        <td valign="top" width="378">
                                                            <?php $view['slots']->output('content3'); ?>
                                                        </td>
                                                        <td valign="top" width="246">
														
														
														
														<?php $view['slots']->output('img11'); ?>
														
														</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/section 3-->
                                    <!--line break-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td height="30" valign="middle" width="624">
											
											<div class="linebreak"></div>
											
											</td>
                                        </tr>
                                    </table>
                                    <!--/line break-->
                                    <!--footer-->
                                    <table cellspacing="0" border="0" cellpadding="0" width="624">
                                        <tr>
                                            <td>
                                                <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
												
												<?php $view['slots']->output('content4'); ?>
												
												</p>
                                                <p style="font-size: 17px; line-height: 24px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
												
												<div class="spacerten"></div>
												
												</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--/footer-->
                                </td>
                            </tr>
                        </table>
                        <!--/email content-->
                    </td>
                </tr>
            </table>
            <!--/email container-->
        </td>
    </tr>
</table>
<!--/100% body table-->

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>