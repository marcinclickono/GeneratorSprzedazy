<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
$view->extend(":$template:base.html.php");
$parentVariant = $page->getVariantParent();
$title         = (!empty($parentVariant)) ? $parentVariant->getTitle() : $page->getTitle();
$view['slots']->set('public', (isset($public) && $public === true) ? true : false);
$view['slots']->set('pageTitle', $title);
?>
 <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="navbar-brand page-scroll" href="#page-top">
                    <?php $view['slots']->output('top1'); ?>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <div class="nav navbar-nav">
					<?php $view['slots']->output('top2'); ?>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
	
	 <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading"><?php $view['slots']->output('top3'); ?></h1>
                        <p class="intro-text"><?php $view['slots']->output('top4'); ?></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </header>
	
	 <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2><?php $view['slots']->output('top5'); ?></h2>
                <p><?php $view['slots']->output('top6'); ?></p>
            </div>
        </div>
    </section>
	
	<!-- Download Section -->
    <section id="download" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2><?php $view['slots']->output('bottom1'); ?></h2>
                    <p><?php $view['slots']->output('bottom2'); ?></p>
                </div>
            </div>
        </div>
    </section>
	
	 <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2><?php $view['slots']->output('bottom3'); ?></h2>
                <p><?php $view['slots']->output('bottom4'); ?></p>
                <p><?php $view['slots']->output('bottom5'); ?></p>
            </div>
        </div>
    </section>
	
	
	
	<footer>
        <div class="container text-center">
            <p><?php $view['slots']->output('bottom6'); ?></p>
        </div>
    </footer>
	
<?php $view['slots']->output('builder'); ?>	
	
	
	
	