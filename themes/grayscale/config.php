<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

$config = array(
    "name"        => "Grayscale",
    "features"    => array(
        "page"
    ),
    "slots"       => array(
        "page" => array(
            "top1",
            "top2",
            "top3",
			"top4",
			"top5",
			"top6",
            "bottom1",
            "bottom2",
            "bottom3",
			"bottom4",
			"bottom5",
			"bottom6",
			"bottom7",
			"bottom8",
			"bottom9",
			"bottom10",
			"bottom11",
			"bottom12",
			"bottom13",
			"bottom14",
			"bottom15",
			"bottom16",
			"bottom17"
        ),
        "email" => array(
            "header",
            "body",
            "footer"
        )
    )
);

return $config;