<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
 
$view->extend(":$template:base.html.php");
$parentVariant = $page->getVariantParent();
$title         = (!empty($parentVariant)) ? $parentVariant->getTitle() : $page->getTitle();
$view['slots']->set('public', (isset($public) && $public === true) ? true : false);
$view['slots']->set('pageTitle', $title);
?>

<nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
				<?php $view['slots']->output('top1'); ?>
				</div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="nav navbar-nav navbar-right">
                  <?php $view['slots']->output('top2'); ?>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
</nav>

<!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" alt="">
                    <div class="intro-text">
                        <span class="name"><?php $view['slots']->output('top3'); ?></span>
                        <hr class="star-light">
                        <span class="skills"><?php $view['slots']->output('top4'); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
	
	 <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            
            <div class="row">
                
                <div class="col-sm-4 portfolio-item">
                  <?php $view['slots']->output('bottom1'); ?>
                </div>
                <div class="col-sm-4 portfolio-item">
                  <?php $view['slots']->output('bottom2'); ?>
                </div>
                <div class="col-sm-4 portfolio-item">
                  <?php $view['slots']->output('bottom3'); ?>
                </div>
                <div class="col-sm-4 portfolio-item">
                   <?php $view['slots']->output('bottom4'); ?>
                </div>
                <div class="col-sm-4 portfolio-item">
					<?php $view['slots']->output('bottom5'); ?>
                </div>
				<div class="col-sm-4 portfolio-item">
                  <?php $view['slots']->output('bottom6'); ?>
                </div>
            </div>
        </div>
    </section>
	
	<section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php $view['slots']->output('bottom7'); ?></h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <?php $view['slots']->output('bottom8'); ?>
                </div>
                <div class="col-lg-4">
                    <?php $view['slots']->output('bottom9'); ?>
                </div>
                
            </div>
        </div>
    </section>
	
	<!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                   <?php $view['slots']->output('bottom10'); ?>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">

                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                      
                        <br>
                        <div id="success"></div>
                </div>
            </div>
        </div>
    </section>
	
	    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
						<?php $view['slots']->output('bottom13'); ?>
                    </div>
                    <div class="footer-col col-md-4">
						<?php $view['slots']->output('bottom14'); ?>
                    </div>
                    <div class="footer-col col-md-4">
                        <?php $view['slots']->output('bottom15'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
						<?php $view['slots']->output('bottom16'); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	
	<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
	
		 <!-- Plugin JavaScript -->
   

<?php $view['slots']->output('builder'); ?>



