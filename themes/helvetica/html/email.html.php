<?php
/**
 * @package     Mautic
 * @copyright   2014 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<html>
<head>
	<link rel="stylesheet" href="<?php echo $view['assets']->getUrl('themes/helvetica/css/helvetica.css'); ?>" type="text/css" />
    <?php $view['assets']->outputHeadDeclarations(); ?>
</head>
<body style="background: #161616;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" bgcolor="#161616"><table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="font: 14px Helvetica, Arial, sans-serif; color: #767572; line-height: 100%;">
      <tr>
        <td valign="top"><table width="600" height="204" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td style="height: 63px;"><table width="100%" border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; font-size: 11px; color: #808080; padding-top: 15px;"> </webversion>
                  <?php $view['slots']->output('top1'); ?>
                  <unsubscribe></unsubscribe></td>
              </tr>
            </table></td>
            <td style="height: 56px;"><div class="imgarrow" style="display: block;"></div></td>
          </tr>
          <tr>
            <td colspan="2"><div class="footergif" style="display: block;"></div></td>
          </tr>
          <tr>
            <td valign="top" style="width: 504px; height: 125px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="height: 125px;">
              <tr>
                <td valign="top" bgcolor="#161616" style="height: 10px;">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" bgcolor="#161616" style="height: 50px; font-family: Helvetica, Arial, sans-serif; font-size: 55px; font-weight: bold; color: #f9f8f2; letter-spacing: -2px; line-height: 90%;">
				
				<?php $view['slots']->output('top2'); ?>

				</td>
              </tr>
              <tr>
                <td valign="top" bgcolor="#161616" style="height: 25px; font-family: Helvetica, Arial, sans-serif; font-size: 19px; font-weight: bold; color: #4e4e4e; letter-spacing: -2px;"><?php $view['slots']->output('top3'); ?></td>
              </tr>
            </table></td>
            <td valign="top">
				<?php $view['slots']->output('img0'); ?>
			</td>
          </tr>
          <tr>
            <td colspan="2" valign="top" class="headerborder" style="height: 16px;">
				<div class="footergif"></div>
			</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" bgcolor="#161616"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" style="height: 35px;">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; font-size: 30px; font-weight: bold; color: #767572; letter-spacing: -2px;">
					<?php $view['slots']->output('top4'); ?>
				</td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; color: #767572;">
					<?php $view['slots']->output('img1'); ?>
				</td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; font-size: 30px; font-weight: bold; color: #767572; letter-spacing: -2px;">
					<?php $view['slots']->output('top5'); ?>
				</td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; color: #767572;">
					<?php $view['slots']->output('top6'); ?>
				</td>
              </tr>
              <tr>
                <td style="height: 40px;">&nbsp;</td>
              </tr>
              <tr>
                <td><div class="footergif"></div>
              </tr>
              <tr>
                <td style="height: 20px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="height: 30px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 25px; font-weight: bold; color: #767572; letter-spacing: -2px; padding-bottom: 10px;"><?php $view['slots']->output('bottom1'); ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572; line-height: 130%;"><?php $view['slots']->output('bottom2'); ?>
	
						</td>
                      </tr>
                    </table></td>
                    <td width="5%" valign="top">&nbsp;</td>
                    <td width="30%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 25px; font-weight: bold; color: #767572; letter-spacing: -2px; padding-bottom: 10px;"><?php $view['slots']->output('bottom3'); ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572; line-height: 130%;"><?php $view['slots']->output('bottom4'); ?>
						
						</td>
                      </tr>
                    </table></td>
                    <td width="5%" valign="top">&nbsp;</td>
                    <td width="30%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 25px; font-weight: bold; color: #767572; letter-spacing: -2px; padding-bottom: 10px;"><?php $view['slots']->output('bottom5'); ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572; line-height: 130%;">
						<?php $view['slots']->output('bottom6'); ?>				
						</td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td><table width="600" border="0" cellpadding="0" cellspacing="0" style="padding-top: 10px;">
          <tr>
            <td colspan="6"><div class="footergif"></div>
            <td width="1" rowspan="3">
			
			<div class="footerimg"></div>
			
			
			</td>
          </tr>
          <tr>
            <td width="179" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="21%" valign="top">
					<div class="emailfriend" width="77" height="77"></div>
				</td>
                <td width="10%" valign="top">&nbsp;</td>
                <td width="69%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 17px; font-weight: bold; color: #f9f8f2;"><?php $view['slots']->output('bottom7'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; color: #767572;"><?php $view['slots']->output('bottom8'); ?>
                    </td>
                  </tr>
                </table></td>
              </tr>
            </table>
              <p style="font-size: 16px; font-weight: bold; margin: 0; padding: 0 0 6px 0;">&nbsp;</p></td>
            <td width="30" valign="top">&nbsp;</td>
            <td width="36" rowspan="2" valign="top"><div class="iconfooter"></div></td>
            <td width="16" rowspan="2" valign="top">&nbsp;</td>
            <td width="334" rowspan="2" valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; color: #767572; margin: 0; padding: 0 0 6px 0;"><?php $view['slots']->output('bottom9'); ?></p>
              <p style="font-family: Helvetica, Arial, sans-serif; font-size: 11px; color: #767572; margin: 0; padding: 0; letter-spacing: -0.3px;"><?php $view['slots']->output('bottom10'); ?></p>
			  
			  </td>
          </table></td>
      </tr>
    </table></td>
  </tr>
</table>  

<?php $view['slots']->output('builder'); ?>	
	
	
</body>
</html>