<?php
$config = array(
	'name' => 'English (United Kingdom)',
	'locale' => 'en_GB',
	'author' => 'Mautic Translators',
);

return $config;