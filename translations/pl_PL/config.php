<?php
$config = array(
	'name' => 'Polish (Poland)',
	'locale' => 'pl_PL',
	'author' => 'Mautic Translators',
);

return $config;